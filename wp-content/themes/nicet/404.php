<?php
/**
 * The template for displaying the 404 template in the Twenty Twenty theme.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */
get_header();
?>

<main id="site-content" role="main">
	<div class="page-title secondary-bg white-text">
		<div class="container">
			<h1 class="entry-title pt-3 pb-3"><?php _e( 'Page Not Found', 'nicet' ); ?></h1>
		</div>
	</div>
	<div class="container">
		<div class="section-inner thin error404-content">
			<div class="text-center">
				<div class="icon-container">
					<i class="fas fa-book"></i>
				</div>
				<h2 class="mb-5">Seems you are lost.</h2>
			</div>

		</div><!-- .section-inner -->
	</div>
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php
get_footer();
