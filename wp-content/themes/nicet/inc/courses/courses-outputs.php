<?php

class Courses_type_widget extends WP_Widget {

	public $arg = array(
		'taxonomy' => 'coursetype',
		'orderby' => 'name',
		'order' => 'ASC',
		'hide_empty' => 0
	);

	public function __construct() {
		$widget_ops = array(
			'classname' => 'course_types',
			'description' => __( 'Get List of Course Types' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'ct_list', 'Course Types', $widget_ops );
		$this->alt_option_name = 'course_types';
	}

	public function widget( $args, $instance ) {
		if ( !isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		$title = (!empty( $instance['title'] ) ) ? $instance['title'] : __( 'Get Course Types' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$tax_selector = isset( $instance['taxselector'] ) ? esc_attr( $instance['taxselector'] ) : '';
		$title_check = isset( $instance['title_check'] ) ? esc_attr( $instance['title_check'] ) : '';

		if ( $tax_selector != 0 ) {
			$extra_args = array( 'parent' => $tax_selector );
			$args = array_merge( $this->arg, array( 'parent' => $tax_selector ) );
		} else {
			$args = array_merge( $this->arg, array( 'parent' => 0 ) );
		}

		echo $args['before_widget'];
		if ( $title && $title_check == '' ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$taxs = get_categories( $args );
		if ( !empty( $taxs ) && !is_wp_error( $taxs ) ) {
			?>
			<ul class="parent tax-menu">
				<?php
				foreach ( $taxs as $tax ) {
					?>

					<li>
						<a href="<?php echo get_category_link( $tax->term_id ) ?>">
							<?php echo $tax->name; ?>
						</a>
						<?php
						$this->subtax( $tax->term_id );
						?>
					</li>
					<?php
				}
				?>
			</ul>
			<?php
		}
		echo $args['after_widget'];
	}

	public function subtax( $parentId ) {
		if ( isset( $parentId ) ) {
			$subtax = array_merge( $this->arg, array( 'parent' => $parentId ) );

			$taxs = get_categories( $subtax );
			if ( !empty( $taxs ) && !is_wp_error( $taxs ) ) {
				?>
				<ul class="child tax-sub-menu">
					<?php
					foreach ( $taxs as $tax ) {
						?>

						<li>
							<a href="<?php echo get_category_link( $tax->term_id ) ?>">
								<?php echo $tax->name; ?>
							</a>
							<?php
							$this->subtax( $tax->term_id );
							?>
						</li>
						<?php
					}
					?>
				</ul>
				<?php
			}
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['taxselector'] = $new_instance['taxselector'];
		$instance['title_check'] = (int) $new_instance['title_check'];
		return $instance;
	}

	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$tax_selector = isset( $instance['taxselector'] ) ? esc_attr( $instance['taxselector'] ) : 0;
		$title_check = (isset( $instance['title_check'] )) ? 'Yes' : 'No';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input 
				class="widefat"
				id="<?php echo $this->get_field_id( 'title' ); ?>"
				name="<?php echo $this->get_field_name( 'title' ); ?>"
				type="text"
				value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'title_check' ); ?>"><?php _e( 'Hide title?' ); ?></label>
			<input 
				id="<?php echo $this->get_field_id( 'title_check' ); ?>"
				name="<?php echo $this->get_field_name( 'title_check' ); ?>"
				type="checkbox"
				<?php checked( $title_check, 'Yes' ); ?>
				value="Yes"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'taxselector' ); ?>"><?php _e( 'Select Parent Course Type' ); ?></label>
			<select
				class="tiny-text"
				id="<?php echo $this->get_field_id( 'taxselector' ); ?>"
				name="<?php echo $this->get_field_name( 'taxselector' ); ?>"
				>
				<option value="0">All</option>
				<?php
				$args = array(
					'taxonomy' => 'coursetype',
					'orderby' => 'name',
					'order' => 'ASC',
					'hide_empty' => 0,
					'parent' => 0
				);

				$taxs = get_categories( $args );

				foreach ( $taxs as $tax ) {
					?>
					<option 
						value="<?php echo $tax->term_id; ?>" 
						<?php selected( $tax_selector, $tax->term_id ); ?>
						><?php echo esc_attr( $tax->name ); ?></option>
						<?php
					}
					?>
			</select>
		</p>
		<?php
	}

}

add_action( 'widgets_init', function() {
	register_widget( 'Courses_type_widget' );
} );
