<?php

if ( !ABSPATH )
	exit;

function create_blog_cpt() {

	$labels = array(
		'name' => _x( 'Blogs', 'Post Type General Name', 'nicet' ),
		'singular_name' => _x( 'Blog', 'Post Type Singular Name', 'nicet' ),
		'menu_name' => _x( 'Blogs', 'Admin Menu text', 'nicet' ),
		'name_admin_bar' => _x( 'Blog', 'Add New on Toolbar', 'nicet' ),
		'archives' => __( 'Blog Archives', 'nicet' ),
		'attributes' => __( 'Blog Attributes', 'nicet' ),
		'parent_item_colon' => __( 'Parent Blog:', 'nicet' ),
		'all_items' => __( 'All Blogs', 'nicet' ),
		'add_new_item' => __( 'Add New Blog', 'nicet' ),
		'add_new' => __( 'Add New', 'nicet' ),
		'new_item' => __( 'New Blog', 'nicet' ),
		'edit_item' => __( 'Edit Blog', 'nicet' ),
		'update_item' => __( 'Update Blog', 'nicet' ),
		'view_item' => __( 'View Blog', 'nicet' ),
		'view_items' => __( 'View Blogs', 'nicet' ),
		'search_items' => __( 'Search Blog', 'nicet' ),
		'not_found' => __( 'Not found', 'nicet' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'nicet' ),
		'featured_image' => __( 'Featured Image', 'nicet' ),
		'set_featured_image' => __( 'Set featured image', 'nicet' ),
		'remove_featured_image' => __( 'Remove featured image', 'nicet' ),
		'use_featured_image' => __( 'Use as featured image', 'nicet' ),
		'insert_into_item' => __( 'Insert into Blog', 'nicet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Blog', 'nicet' ),
		'items_list' => __( 'Blogs list', 'nicet' ),
		'items_list_navigation' => __( 'Blogs list navigation', 'nicet' ),
		'filter_items_list' => __( 'Filter Blogs list', 'nicet' ),
	);
	$args = array(
		'label' => __( 'Blog', 'nicet' ),
		'description' => __( '', 'nicet' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-aside',
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'author', 'comments', 'trackbacks', 'post-formats' ),
		'taxonomies' => array( 'blogcategories', 'blogtags' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'blog', $args );
}

add_action( 'init', 'create_blog_cpt', 0 );

// Register Taxonomy Blog Category
function create_blogcategory_tax() {

	$labels = array(
		'name' => _x( 'Blog Categories', 'taxonomy general name', 'nicet' ),
		'singular_name' => _x( 'Blog Category', 'taxonomy singular name', 'nicet' ),
		'search_items' => __( 'Search Blog Categories', 'nicet' ),
		'all_items' => __( 'All Blog Categories', 'nicet' ),
		'parent_item' => __( 'Parent Blog Category', 'nicet' ),
		'parent_item_colon' => __( 'Parent Blog Category:', 'nicet' ),
		'edit_item' => __( 'Edit Blog Category', 'nicet' ),
		'update_item' => __( 'Update Blog Category', 'nicet' ),
		'add_new_item' => __( 'Add New Blog Category', 'nicet' ),
		'new_item_name' => __( 'New Blog Category Name', 'nicet' ),
		'menu_name' => __( 'Blog Category', 'nicet' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'nicet' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'show_in_rest' => true,
		'can_export' => true,
	);
	register_taxonomy( 'blogcategory', array( 'blog' ), $args );
}

add_action( 'init', 'create_blogcategory_tax' );

// Register Taxonomy Blog Tag
function create_blogtag_tax() {

	$labels = array(
		'name' => _x( 'Blog Tags', 'taxonomy general name', 'nicet' ),
		'singular_name' => _x( 'Blog Tag', 'taxonomy singular name', 'nicet' ),
		'search_items' => __( 'Search Blog Tags', 'nicet' ),
		'all_items' => __( 'All Blog Tags', 'nicet' ),
		'parent_item' => __( 'Parent Blog Tag', 'nicet' ),
		'parent_item_colon' => __( 'Parent Blog Tag:', 'nicet' ),
		'edit_item' => __( 'Edit Blog Tag', 'nicet' ),
		'update_item' => __( 'Update Blog Tag', 'nicet' ),
		'add_new_item' => __( 'Add New Blog Tag', 'nicet' ),
		'new_item_name' => __( 'New Blog Tag Name', 'nicet' ),
		'menu_name' => __( 'Blog Tag', 'nicet' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'nicet' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'show_in_rest' => true,
		'can_export' => true,
	);
	register_taxonomy( 'blogtag', array( 'blog' ), $args );
}

add_action( 'init', 'create_blogtag_tax' );
