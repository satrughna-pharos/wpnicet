<?php
if ( !ABSPATH )
	exit;

function register_shortcodes() {
	add_shortcode( 'blog-sidebar', 'nicet_blogslider' );
}

add_action( 'init', 'register_shortcodes' );

function nicet_blogslider() {
	extract( shortcode_atts( array( 'posts_per_page' => 5, ), $atts ) );

	global $wp_query, $paged, $post;

	$temp = $wp_query;
	$wp_query = null;
	$wp_query = new WP_Query();

	$blogquery = 'post_type=blog&orderby=date&order=DESC';

	if ( !empty( $posts_per_page ) ) {
		$blogquery .= '&posts_per_page=' . $posts_per_page;
	}

	$wp_query->query( $blogquery );
	ob_start();

	if ( $wp_query->have_posts() ) :
		?>
		<div class="bs-container">
			<?php
			while ( $wp_query->have_posts() ) :
				$wp_query->the_post();
				?>
				<div class="sc-blog-item">
					<div class="blog-entry">
						<a href="<?php the_permalink(); ?>" class="block-20 d-flex align-items-end">
							<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'full', array( 'class' => 'responsive-img' ) );
							} else {
								?>
								<img src="<?php bloginfo( 'template_directory' ); ?>/images/no-image.png" class="responsive-img center" alt="No Thumbnail"/>
								<?php
							}
							?>
							<span class="meta-date text-center p-2">
								<span class="day"><?php echo get_the_date( 'j' ); ?></span>
								<span class="mos"><?php echo get_the_date( 'M' ); ?></span>
								<span class="yr"><?php echo get_the_date( 'Y' ); ?></span>
							</span>
						</a>
						<div class="text bg-white p-4">
							<h5 class="heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
							<p><?php echo wp_trim_words( get_the_content(), 10, '... <a href="' . get_permalink() . '">Read More</a>' ) ?></p>
							<div class="d-flex align-items-center mt-4">
								<p class="ml-auto mb-0"><strong>Author:</strong> <?php the_author(); ?></p>
							</div>
						</div>
					</div>
				</div>
				<?php
			endwhile;
			?>
		</div>
		<?php
	endif;
	$wp_query = null;
	$wp_query = $temp;

	$content = ob_get_contents();

	ob_end_clean();

	return $content;
}

/* * ** Widget for Blog slider *** */

class Blog_slider_widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_blog_slider',
			'description' => __( 'Latest Blog Posts.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'blog-slider', __( 'Blog Slider' ), $widget_ops );
		$this->alt_option_name = 'widget_blog_slider';
	}

	public function widget( $args, $instance ) {
		if ( !isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = (!empty( $instance['title'] ) ) ? $instance['title'] : __( 'Latest Blogs' );

		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = (!empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( !$number ) {
			$number = 5;
		}
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$r = new WP_Query(
				apply_filters(
						'widget_posts_args',
						array(
							'post_type' => 'blog',
							'posts_per_page' => $number,
							'no_found_rows' => true,
							'post_status' => 'publish',
							'ignore_sticky_posts' => false,
						),
						$instance
				)
		);

		if ( !$r->have_posts() ) {
			return;
		}
		?>
		<?php echo $args['before_widget']; ?>
		<?php
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		?>

		<?php
		if ( $r->have_posts() ) :
			?>
			<div class="bs-container">
				<?php
				while ( $r->have_posts() ) :
					$r->the_post();
					?>
					<div class="sc-blog-item">
						<div class="blog-entry">
							<a href="<?php the_permalink(); ?>" class="block-20 d-flex align-items-end">
								<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail( 'full', array( 'class' => 'responsive-img' ) );
								} else {
									?>
									<img src="<?php bloginfo( 'template_directory' ); ?>/images/no-image.png" class="responsive-img center" alt="No Thumbnail"/>
									<?php
								}
								?>
								<span class="meta-date text-center p-2">
									<span class="day"><?php echo get_the_date( 'j' ); ?></span>
									<span class="mos"><?php echo get_the_date( 'M' ); ?></span>
									<span class="yr"><?php echo get_the_date( 'Y' ); ?></span>
								</span>
							</a>
							<div class="text bg-white">
								<h5 class="heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
								<p><?php echo wp_trim_words( get_the_content(), 10, '... <a href="' . get_permalink() . '">Read More</a>' ) ?></p>
								<div class="d-flex align-items-center mt-4">
									<p class="ml-auto mb-0"><strong>Author:</strong> <?php the_author(); ?></p>
								</div>
							</div>
						</div>
					</div>
					<?php
				endwhile;
				?>
			</div>
			<?php
		endif;
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		return $instance;
	}

	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
			<input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox"<?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?' ); ?></label></p>
		<?php
	}

}

add_action( 'widgets_init', function() {
	register_widget( 'Blog_slider_widget' );
} );

class Blog_listing_widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_blog_list',
			'description' => __( 'Custom Blog Listing.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'blog-listing', __( 'Custom Blog Listing' ), $widget_ops );
		$this->alt_option_name = 'widget_blog_slider';
	}

	public function widget( $args, $instance ) {
		if ( !isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = (!empty( $instance['title'] ) ) ? $instance['title'] : __( 'Latest Blogs' );

		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = (!empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( !$number ) {
			$number = 5;
		}
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$r = new WP_Query(
				apply_filters(
						'widget_posts_args',
						array(
							'post_type' => 'blog',
							'posts_per_page' => $number,
							'no_found_rows' => true,
							'post_status' => 'publish',
							'ignore_sticky_posts' => false,
						),
						$instance
				)
		);

		if ( !$r->have_posts() ) {
			return;
		}
		?>
		<?php echo $args['before_widget']; ?>
		<?php
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		?>

		<?php
		if ( $r->have_posts() ) :
			?>
			<ul class="bl-container">
				<?php
				while ( $r->have_posts() ) :
					$r->the_post();
					?>
					<li class="bl-blog-item">
						<div class="bl-blog-image">
							<a href="<?php the_permalink(); ?>" class="block-20 d-flex align-items-end">
								<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail( 'full', array( 'class' => 'responsive-img' ) );
								} else {
									?>
									<img src="<?php bloginfo( 'template_directory' ); ?>/images/no-image.png" class="responsive-img center" alt="No Thumbnail"/>
									<?php
								}
								?>
							</a>
						</div>
						<div class="bl-blog-content">
							<h5 class="heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
							<div class="bl-meta-date"><span class="day"><?php echo get_the_date( 'j - M - Y' ); ?></span></div>
						</div>
					</li>
					<?php
				endwhile;
				?>
			</ul>
			<?php
		endif;
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		return $instance;
	}

	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
			<input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox"<?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?' ); ?></label></p>
		<?php
	}

}

add_action( 'widgets_init', function() {
	register_widget( 'Blog_listing_widget' );
} );
