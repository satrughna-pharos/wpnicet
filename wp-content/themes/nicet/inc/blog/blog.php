<?php

if ( !ABSPATH )
	exit;

include_once( dirname( __FILE__ ) . '/blog-posttype.php' );
include_once( dirname( __FILE__ ) . '/blog-outputs.php' );
