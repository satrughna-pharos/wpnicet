<?php

Kirki::add_config( 'theme_config_id', array(
	'capability' => 'edit_theme_options',
	'option_type' => 'theme_mod',
) );

Kirki::add_panel( 'theme_options_panel', array(
	'priority' => 10,
	'title' => esc_html__( 'Custom Options', 'kirki' ),
	'description' => esc_html__( 'Theme customization options', 'kirki' ),
) );

Kirki::add_section( 'image_identities', array(
	'title' => esc_html__( 'Identities', 'kirki' ),
	'description' => esc_html__( 'Theme Identity controls', 'kirki' ),
	'panel' => 'theme_options_panel',
	'priority' => 160,
) );
Kirki::add_section( 'theme_colors', array(
	'title' => esc_html__( 'Colors', 'kirki' ),
	'description' => esc_html__( 'Theme color controls', 'kirki' ),
	'panel' => 'theme_options_panel',
	'priority' => 160,
) );
Kirki::add_section( 'social_links', array(
	'title' => esc_html__( 'Scial Links', 'kirki' ),
	'description' => esc_html__( 'Add Social Links', 'kirki' ),
	'panel' => 'theme_options_panel',
	'priority' => 160,
) );


//Identities
Kirki::add_field( 'theme_config_id', [
	'type' => 'image',
	'settings' => 'site_logo',
	'label' => esc_html__( 'Logo', 'kirki' ),
	'description' => esc_html__( 'Upload your logo.', 'kirki' ),
	'section' => 'image_identities',
	'default' => '',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'image',
	'settings' => 'site_favicon',
	'label' => esc_html__( 'Favicon', 'kirki' ),
	'description' => esc_html__( 'Upload your Favicon.', 'kirki' ),
	'section' => 'image_identities',
	'default' => '',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'text',
	'settings' => 'basicphone',
	'label' => esc_html__( 'Phone Number', 'kirki' ),
	'description' => esc_html__( 'Enter your Phone Number.', 'kirki' ),
	'section' => 'image_identities',
	'default' => '',
	'priority' => 10,
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'text',
	'settings' => 'basicemail',
	'label' => esc_html__( 'Email ID', 'kirki' ),
	'description' => esc_html__( 'Enter your Email Address.', 'kirki' ),
	'section' => 'image_identities',
	'default' => '',
	'priority' => 10,
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'text',
	'settings' => 'copyright_text',
	'label' => esc_html__( 'Copyright Text', 'kirki' ),
	'description' => esc_html__( 'Enter your copyright text.', 'kirki' ),
	'section' => 'image_identities',
	'default' => '',
	'priority' => 10,
] );

//Color and Backgrounds
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'primarycolor',
	'label' => __( 'Primary Color', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#FD5F00',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'secondarycolor',
	'label' => __( 'Secondary Color', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#0E1229',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'headerbg',
	'label' => __( 'Top Header Background', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#003869',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'headertext',
	'label' => __( 'Top Header Text Color', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#fff',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'headerlink',
	'label' => __( 'Top Header Link Color', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#fff',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'footerbg',
	'label' => __( 'Footer Background', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#0088CC',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'footertext',
	'label' => __( 'Footer Text Color', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#fff',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'footerlink',
	'label' => __( 'Footer Link Color', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#fff',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'copyrightbg',
	'label' => __( 'Copyright Background', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#0088CC',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'copyrighttext',
	'label' => __( 'Copyright Text Color', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#fff',
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'color',
	'settings' => 'copyrightlink',
	'label' => __( 'Copyright Link Color', 'kirki' ),
	'description' => esc_html__( '', 'kirki' ),
	'section' => 'theme_colors',
	'default' => '#fff',
] );

// Social Media section
Kirki::add_field( 'theme_config_id', [
	'type' => 'text',
	'settings' => 'facebook_link',
	'label' => esc_html__( 'Facebook', 'kirki' ),
	'section' => 'social_links',
	'default' => esc_html__( 'https://', 'kirki' ),
	'priority' => 10,
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'text',
	'settings' => 'twitter_link',
	'label' => esc_html__( 'Twitter', 'kirki' ),
	'section' => 'social_links',
	'default' => esc_html__( 'https://', 'kirki' ),
	'priority' => 10,
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'text',
	'settings' => 'linkedin_link',
	'label' => esc_html__( 'LinkedIn', 'kirki' ),
	'section' => 'social_links',
	'default' => esc_html__( 'https://', 'kirki' ),
	'priority' => 10,
] );
Kirki::add_field( 'theme_config_id', [
	'type' => 'text',
	'settings' => 'instagram_link',
	'label' => esc_html__( 'Instagram', 'kirki' ),
	'section' => 'social_links',
	'default' => esc_html__( 'https://', 'kirki' ),
	'priority' => 10,
] );