<?php

if ( !ABSPATH )
	exit;

include_once( dirname( __FILE__ ) . '/kirki/kirki.php' );
include_once( dirname( __FILE__ ) . '/kirkicustomizer.php' );
include_once( dirname( __FILE__ ) . '/blog/blog.php' );
include_once( dirname( __FILE__ ) . '/courses/courses.php' );

/**
 * Disable Notification for specific plugins
 */
function filter_plugin_updates( $value ) {
	unset( $value->response['akismet/akismet.php'] );
	unset( $value->response['album-and-image-gallery-plus-lightbox/album-and-image-gallery.php'] );
	return $value;
}

add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );
