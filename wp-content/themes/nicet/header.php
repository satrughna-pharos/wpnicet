<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title><?php wp_title( '' ); ?></title>
		<link rel="shortcut icon" href="<?php echo get_theme_mod( 'site_favicon' ) . v(); ?>" />
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_theme_mod( 'site_favicon' ) . v(); ?>" />
		<link rel="apple-touch-icon" href="<?php echo get_theme_mod( 'site_favicon' ) . v(); ?>" />
		<link href="<?php bloginfo( 'template_directory' ); ?>/css/theme.css<?php echo v(); ?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="<?php echo bloginfo( 'stylesheet_url' ); ?><?php echo v(); ?>"/>
		<?php
		$basicphone = get_theme_mod( 'basicphone' );
		$basicemail = get_theme_mod( 'basicemail' );
		$facebook_link = get_theme_mod( 'facebook_link' );
		$twitter_link = get_theme_mod( 'twitter_link' );
		$linkedin_link = get_theme_mod( 'linkedin_link' );
		$instagram_link = get_theme_mod( 'instagram_link' );

		$primary_color = get_theme_mod( 'primarycolor', '#FD5F00' );
		$secondary_color = get_theme_mod( 'secondarycolor', '#0E1229' );
		$headerbg_color = get_theme_mod( 'headerbg', '#ffffff' );
		$headertext = get_theme_mod( 'headertext', $secondary_color );
		$headerlink = get_theme_mod( 'headerlink', $primary_color );
		$footerbg = get_theme_mod( 'footerbg' );
		$footertext = get_theme_mod( 'footertext' );
		$footerlink = get_theme_mod( 'footerlink' );
		$copyrightbg = get_theme_mod( 'copyrightbg' );
		$copyrighttext = get_theme_mod( 'copyrighttext' );
		$copyrightlink = get_theme_mod( 'copyrightlink' );
		?>
		<style>
			:root{
				--primary-color: <?php echo $primary_color; ?>;
				--secondary-color: <?php echo $secondary_color; ?>;
				--header-bg: <?php echo $headerbg_color; ?>;
				--header-text: <?php echo $headertext; ?>;
				--header-link: <?php echo $headerlink; ?>;
				--footer-bg: <?php echo $footerbg; ?>;
				--footer-text: <?php echo $footertext; ?>;
				--footer-link: <?php echo $footerlink; ?>;
				--copyright-bg: <?php echo $copyrightbg; ?>;
				--copyright-text: <?php echo $copyrighttext; ?>;
				--copyright-link: <?php echo $copyrightlink; ?>;
			}
			.primary-bg {
				background-color: var(--primary-color);
			}
			.secondary-bg {
				background-color: var(--secondary-color);
			}
			.primary-bg-hover:hover {
				background-color: var(--primary-color);
			}
			.secondary-bg-hover:hover {
				background-color: var(--secondary-color);
			}
			.primary-color {
				color: var(--primary-color);
			}		
			.secondary-color {
				color: var(--secondary-color);
			}
			.primary-color-hover:hover {
				color: var(--primary-color);
			}		
			.secondary-color-hover:hover {
				color: var(--secondary-color);
			}
			body {
				color: var(--secondary-color);
			}
			.header-bg {
				background-color: var(--header-bg);
				color: var(--header-text);
			}
			.header-bg a {
				color: var(--header-link);
			}
			.footer {
				background-color: var(--footer-bg);
				color: var(--footer-text);
			}
			.footer a {
				color: var(--footer-link);
			}
			.copyright {
				/*background-color: var(--copyright-bg);*/
				color: var(--copyright-text);
			}
			.copyright a {
				color: var(--copyright-link);
			}
			header #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item.mega-current-menu-item > a.mega-menu-link {
				color: var(--primary-color);
			}
		</style>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<header id="mainheader">
			<div class="top-logo-section">
				<div class="container">
					<div class="row mb-0">
						<div class="col-lg-3">
							<div class="logo">
								<a href="<?php echo get_site_url(); ?>">
									<?php
									$site_logo = get_theme_mod( 'site_logo' );
									if ( !empty( $site_logo ) ) {
										?>
										<img src="<?php echo $site_logo; ?>" class="responsive-img" alt="NICET Logo">
										<?php
									} else {
										?>
										<img src="<?php bloginfo( 'template_directory' ); ?>/images/nicet-logo.png" class="responsive-img" alt="NICET Logo">
										<?php
									}
									?>
								</a>
							</div>
						</div>
						<div class="col-lg-9 valign-wrapper header-info">
							<div class="right-align text-center text-lg-right top-info">
								<ul class="list-inline header-contacts">
									<?php
									if ( isset( $basicemail ) && $basicemail != '' ) {
										?>
										<li class="list-inline-item">
											<i class="fas fa-phone-alt primary-color"></i> 
											<a href="mailto:<?php echo $basicemail; ?>" class="secondary-color"><?php echo $basicemail; ?></a>
										</li>
										<?php
									}
									if ( isset( $basicphone ) && $basicphone != '' ) {
										?>
										<li class="list-inline-item">
											<i class="fas fa-paper-plane primary-color"></i> 
											<a href="tel:<?php echo $basicphone; ?>" class="secondary-color"><?php echo $basicphone; ?></a>
										</li>
										<?php
									}

									/**
									 * Social Links section begins
									 */
									if ( isset( $facebook_link ) && $facebook_link != '' ) {
										?>
										<li class="list-inline-item">
											<a href="<?php echo $facebook_link; ?>" class="secondary-color">
												<i class="fab fa-facebook-f primary-color"></i>
											</a>
										</li>
										<?php
									}
									if ( isset( $twitter_link ) && $twitter_link != '' ) {
										?>
										<li class="list-inline-item">
											<a href="<?php echo $twitter_link; ?>" class="secondary-color">
												<i class="fab fa-twitter primary-color"></i>
											</a>
										</li>
										<?php
									}
									if ( isset( $linkedin_link ) && $linkedin_link != '' ) {
										?>
										<li class="list-inline-item">
											<a href="<?php echo $linkedin_link; ?>" class="secondary-color">
												<i class="fab fa-linkedin-in primary-color"></i>
											</a>
										</li>
										<?php
									}
									if ( isset( $instagram_link ) && $instagram_link != '' ) {
										?>
										<li class="list-inline-item">
											<a href="<?php echo $instagram_link; ?>" class="secondary-color">
												<i class="fab fa-instagram primary-color"></i>
											</a>
										</li>
										<?php
									}
									/**
									 * Social Links section ended
									 */
									?>
								</ul>
							</div>
							<div class="navigation-area">
								<?php
								if ( has_nav_menu( 'primary' ) ) {
									$primary = array(
										'menu_class' => 'nav-inline',
										'echo' => true,
										'theme_location' => 'primary',
									);
									wp_nav_menu( $primary );
								}
								?>
								<ul class="courses-menu">
									<li>
										<a href="<?php echo get_site_url() ?>/course/">Courses <span class="mega-indicator"></span></a>
										<div class="courses-menu-container">
											<?php
											if ( is_active_sidebar( 'sidebar-4' ) ) {
												dynamic_sidebar( 'sidebar-4' );
											}
											?>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>