<?php

function v() {
	$r1 = rand( 0, 9 );
	$r2 = rand( 10, 99 );
	$r3 = rand( 100, 999 );
	return "?var=" . $r1 . '.' . $r2 . '.' . $r3;
}

add_action( 'init', 'v' );

function ncert_nav_menu() {

	$locations = array(
		'primary' => __( 'Primary Navigation Menu', 'ncert' )
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'ncert_nav_menu' );

function get_featured_image_id( $postID ) {
	global $wpdb;
	$data = $wpdb->get_results( $wpdb->prepare( "SELECT meta_value FROM wp_postmeta WHERE post_id = %d AND meta_key = '_thumbnail_id'", $postID ) );
	if ( !empty( $data[0]->meta_value ) ) {
		return $data[0]->meta_value;
	}
}

include_once( dirname( __FILE__ ) . '/inc/inc.php' );

function nicet_theme_support() {

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'title-tag' );

	add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'script',
				'style',
			)
	);

	load_theme_textdomain( 'nicet' );

	add_theme_support( 'align-wide' );

	if ( is_customize_preview() ) {
		require get_template_directory() . '/inc/starter-content.php';
		add_theme_support( 'starter-content', nicet_get_starter_content() );
	}

	add_theme_support( 'customize-selective-refresh-widgets' );
}

add_action( 'after_setup_theme', 'nicet_theme_support' );

function nicet_create_widget_areas() {
	$common_args = array(
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	register_sidebar(
			array_merge(
					$common_args,
					array(
						'name' => __( 'Footer #1', 'nicet' ),
						'id' => 'sidebar-1',
						'description' => __( 'First Widget Column', 'nicet' ),
					)
			)
	);
	register_sidebar(
			array_merge(
					$common_args,
					array(
						'name' => __( 'Footer #2', 'nicet' ),
						'id' => 'sidebar-2',
						'description' => __( 'Second Widget Column', 'nicet' ),
					)
			)
	);
	register_sidebar(
			array_merge(
					$common_args,
					array(
						'name' => __( 'Footer #3', 'nicet' ),
						'id' => 'sidebar-3',
						'description' => __( 'Third Widget Column', 'nicet' ),
					)
			)
	);
	register_sidebar(
			array_merge(
					$common_args,
					array(
						'name' => __( 'Courses Menu', 'nicet' ),
						'id' => 'sidebar-4',
						'description' => __( 'Top Navigation Extra Category Menu', 'nicet' ),
					)
			)
	);
}

add_action( 'widgets_init', 'nicet_create_widget_areas' );
