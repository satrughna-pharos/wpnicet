<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */
get_header();
?>

<main id="site-content" role="main">
	<?php
	if ( is_post_type_archive( 'blog' ) ) {
		?>
		<div class="page-title secondary-bg white-text">
			<div class="container">
				<h1 class="pt-3 pb-3">Blogs</h1>
			</div>
		</div>
		<?php
	}
	?>
	<div class="container">
		<?php
		$paged = (get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
		$the_query = query_posts(
				array(
					'post_type' => 'blog',
					'posts_per_page' => 12,
					'orderby' => 'date',
					'paged' => $paged
				)
		);

		if ( have_posts() ) {
			?>
			<div class="mgrid">
				<?php
				while ( have_posts() ) {
					the_post();
					?>
					<div class="mgrid-item grid-sizer">
						<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
							<a href="<?php the_permalink(); ?>">
								<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail( 'full', array( 'class' => 'responsive-img' ) );
								}
								?>
							</a>
							<header class="entry-header has-text-align-center<?php echo esc_attr( $entry_header_classes ); ?>">
								<?php the_title( '<h5 class="entry-title text-left heading-size-1"><a href="' . esc_url( get_permalink() ) . '">', '</a></h5>' ); ?>
							</header>
							<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">
								<div class="entry-content">
									<?php
									echo wp_trim_words( get_the_excerpt(), 20, '... <a href="' . get_the_permalink() . '">Read more</a>' );
									?>
								</div>
							</div>
							<div class="section-inner">
								<?php
								wp_link_pages(
										array(
											'before' => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'nicet' ) . '"><span class="label">' . __( 'Pages:', 'nicet' ) . '</span>',
											'after' => '</nav>',
											'link_before' => '<span class="page-number">',
											'link_after' => '</span>',
										)
								);
								edit_post_link();
								if ( is_single() ) {
									get_template_part( 'template-parts/entry-author-bio' );
								}
								?>
							</div>
							<?php
							if ( is_single() ) {
								get_template_part( 'template-parts/navigation' );
							}
							if ( ( is_single() ) && ( comments_open() || get_comments_number() ) && !post_password_required() ) {
								?>
								<div class="comments-wrapper section-inner">
									<?php comments_template(); ?>
								</div>
								<?php
							}
							?>
						</article>
					</div>
					<?php
				}
				?>
			</div>
			<div class="nav-pagination mt-4 mb-4">
				<div class="pagination-item alignleft"><?php previous_posts_link( '<i class="fas fa-angle-left"></i> Newer Articles' ) ?></div>
				<div class="pagination-item alignright"><?php next_posts_link( 'Older Articles <i class="fas fa-angle-right"></i>' ) ?></div>
			</div>
			<?php
		}
		?>
	</div>
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
