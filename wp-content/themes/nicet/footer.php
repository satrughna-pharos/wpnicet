<script src="<?php bloginfo( 'template_directory' ); ?>/js/jquery-3.4.1.min.js" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/popper.min.js<?php echo v(); ?>" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/all.min.js" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/v4-shims.min.js" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/materialize.min.js" type="text/javascript"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/masonry.pkgd.min.js" type="text/javascript"></script>
<?php
if ( is_front_page() ) {
	?>
	<script src="<?php bloginfo( 'template_directory' ); ?>/js/jquery.waypoints.min.js" type="text/javascript"></script>
	<script src="<?php bloginfo( 'template_directory' ); ?>/js/jquery.countup.min.js" type="text/javascript"></script>
	<script>
		jQuery(document).ready(function ($) {
			$('.counter').countUp({
				'time': 800,
				'delay': 10
			});

			$('.bs-container').slick({
				dots: true,
				infinite: true,
				speed: 300,
				slidesToShow: 4,
				slidesToScroll: 4,
				responsive: [
					{
						breakpoint: 1200,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 1,
							infinite: true,
							dots: true
						}
					},
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 576,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				]
			});
		});
	</script>
	<?php
}
?>
<script>
    jQuery(document).ready(function ($) {
        $('.mgrid').masonry({
            itemSelector: '.mgrid-item',
            percentPosition: true,
            gutter: 10
        });
    });
    new AnimOnScroll(document.getElementsByClassName('mgrid'), {
        minDuration: 0.4,
        maxDuration: 0.7,
        viewportFactor: 0.2
    });
</script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/scripts.js<?php echo v(); ?>" type="text/javascript"></script>
<a href="javascript: void(0)" class="gototop"><i class="fas fa-arrow-up"></i></a>
	<?php wp_footer(); ?>
</body>
</html>