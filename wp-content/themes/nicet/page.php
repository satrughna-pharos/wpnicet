<?php
get_header();
?>
<main id="site-content" role="main">
	<?php
	if ( have_posts() ):
		the_post();
		if ( !is_front_page() ) {
			?>
			<div class="page-title secondary-bg white-text">
				<div class="container">
					<h1 class="pt-3 pb-3"><?php the_title(); ?></h1>
				</div>
			</div>
			<?php
		} elseif ( is_post_type_archive( 'blog' ) ) {
			the_title();
		}
		?>
		<div class="container">
			<?php
			the_content();
			?>
		</div>
		<?php
	endif;
	?>
</div>
</main>
<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
<?php get_footer(); ?>