jQuery(document).ready(function ($) {
    $('.effect-container').bind('mouseenter mouseleave', function () {
        $(this).find('.pulse-effect').toggleClass('pulse');
    });

    $('#mainheader').append($('.top-logo-section').clone().addClass('fix-header'));
    $(window).scroll(function () {
        st = $(window).scrollTop();
        if (st > 500) {
            $('.top-logo-section.fix-header').css({
                '-webkit-transform': 'translateY(0)',
                '-moz-transform': 'translateY(0)',
                '-ms-transform': 'translateY(0)',
                '-o-transform': 'translateY(0)',
                'transform': 'translateY(0)'
            });
            $('.gototop').css({
                '-webkit-transform': 'translate(0px)',
                '-moz-transform': 'translate(0px)',
                '-ms-transform': 'translate(0px)',
                '-o-transform': 'translate(0px)',
                'transform': 'translate(0px)'
            });
        } else {
            console.log('Else Part' + $('html body').scrollTop());
            $('.top-logo-section.fix-header').css({
                '-webkit-transform': 'translateY(-100%)',
                '-moz-transform': 'translateY(-100%)',
                '-ms-transform': 'translateY(-100%)',
                '-o-transform': 'translateY(-100%)',
                'transform': 'translateY(-100%)'
            });
            $('.gototop').css({
                '-webkit-transform': 'translateY(100px)',
                '-moz-transform': 'translateY(100px)',
                '-ms-transform': 'translateY(100px)',
                '-o-transform': 'translateY(100px)',
                'transform': 'translateY(100px)'
            });
        }
    });
    $('.gototop').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
    });
    var parentli = $('.courses-menu-container .parent li')
    parentli.has('ul.child').prepend('<i class="arrow-left"></i>');
});