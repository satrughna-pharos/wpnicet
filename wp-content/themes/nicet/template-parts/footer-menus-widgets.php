<?php
/**
 * Displays the menus and widgets at the end of the main element.
 * Visually, this output is presented as part of the footer element.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */
?>
<footer class="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="section-container">
						<?php
						if ( is_active_sidebar( 'sidebar-1' ) ) {
							dynamic_sidebar( 'sidebar-1' );
						}
						?>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="section-container">
						<?php
						if ( is_active_sidebar( 'sidebar-2' ) ) {
							dynamic_sidebar( 'sidebar-2' );
						}
						?>
					</div>
				</div>
				<div class="col-lg-4 full-height light">
					<div class="section-container">
						<?php
						if ( is_active_sidebar( 'sidebar-3' ) ) {
							dynamic_sidebar( 'sidebar-3' );
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright text-center copyright">
		<?php
		$copyright_text = get_theme_mod( 'copyright_text' );
		if ( isset( $copyright_text ) && $copyright_text !== '' ) {
			echo $copyright_text;
		}
		?>
	</div>
</footer>
