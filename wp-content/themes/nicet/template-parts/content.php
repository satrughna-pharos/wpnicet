<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<?php
	get_template_part( 'template-parts/entry-header' );
	if ( !is_search() ) {
		get_template_part( 'template-parts/featured-image' );
	}
	?>
	<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">
		<div class="entry-content">
			<?php
			if ( is_search() || !is_singular() && 'summary' === get_theme_mod( 'blog_content', 'full' ) ) {
				the_excerpt();
			} else {
				the_content( __( 'Continue reading', 'nicet' ) );
			}
			?>
		</div>
	</div>
	<div class="section-inner">
		<?php
		wp_link_pages(
				array(
					'before' => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'nicet' ) . '"><span class="label">' . __( 'Pages:', 'nicet' ) . '</span>',
					'after' => '</nav>',
					'link_before' => '<span class="page-number">',
					'link_after' => '</span>',
				)
		);
		edit_post_link();
		if ( is_single() ) {
			get_template_part( 'template-parts/entry-author-bio' );
		}
		?>
	</div>
	<?php
	if ( is_single() ) {
		get_template_part( 'template-parts/navigation' );
	}
	if ( ( is_single() ) && ( comments_open() || get_comments_number() ) && !post_password_required() ) {
		?>
		<div class="comments-wrapper section-inner">
			<?php comments_template(); ?>
		</div>
		<?php
	}
	?>
</article>
