<?php
get_header();
?>
<main id="site-content" role="main">
	<?php
	if ( have_posts() ):
		the_post();
		?>
		<div class="page-title secondary-bg white-text">
			<div class="container">
				<h1 class="pt-3 pb-3"><?php the_title(); ?></h1>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php
					$course_duration = get_post_meta( $post->ID, 'course_details_' . 'course-duration', true );
					if ( isset( $course_duration ) && $course_duration != '' ) {
						echo '<p><strong>Course Duration</strong> ' . $course_duration . '</p>';
					}
					$lactures = get_post_meta( $post->ID, 'course_details_' . 'number-of-lecturers', true );
					if ( isset( $lactures ) && $lactures != '' ) {
						echo '<p><strong>Number of Teachers</strong> ' . $lactures . '</p><hr>';
					}
					the_content();
					?>
				</div>
				<div class="col-md-4">
					<div class="stickey">
						<div class="post-thumb-container mb-4">
							<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'full', array( 'class' => 'responsive-img center-block' ) );
							}
							?>
						</div>
						<?php echo do_shortcode( '[contact-form-7 id="505" title="Coourse Enquire"]' ); ?>
					</div>
				</div>
			</div>
		</div>
		<?php
	endif;
	?>
</div>
</main>
<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
<?php get_footer(); ?>